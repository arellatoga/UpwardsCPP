#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>

#include "GameObject.hpp"

class Camera {
public:
	Camera();

	void set(sf::RenderWindow&);
	void follow(GameObject&);
	void draw();
	void reset();

private:
	sf::View view;

	int x;
	int y;

	int prev_x;
	int new_x;
	int x_offset;
};
