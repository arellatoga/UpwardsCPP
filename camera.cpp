#include "camera.hpp"

Camera::Camera() {
	view = sf::View(sf::FloatRect(0, 0, 800, 600));
	x = 400;
	y = 300;

	prev_x = x;
	x_offset = 0;
}

void Camera::set(sf::RenderWindow& w) {
	w.setView(view);
}

void Camera::reset() {
	view.reset(sf::FloatRect(0, 0, 800, 600));
}

void Camera::follow(GameObject& obj) {
	int follow_x = obj.coordinates().x;
	int follow_y = obj.coordinates().y;

	prev_x = new_x;
	new_x = follow_x;

	//x_offset = new_x - prev_x > 0 ? 300 + 100 : 300 - 100;

	if (new_x > prev_x) {
		x_offset = 300 - 200;
	}
	else if (new_x < prev_x) {
		x_offset = 300 + 200;
	}

	x += (follow_x - x - x_offset - 64) * 0.1 * 0.5;
	y += (follow_y - y - 300 - 64) * 0.1 * 0.5;

	view = sf::View(sf::FloatRect(x, y, 800, 600));
}
