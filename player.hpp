#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>

#include "collider.hpp"

class Player {
	public:
	Player(Collider*);

	void run(sf::Event&);
	void draw(sf::RenderWindow&);
	void move(float, float);
	Body* returnBody();

	int getWidth();
	int getHeight();

	private:
	sf::RectangleShape sprite;
	sf::Clock deltaClock;

	Collider* col;
	Body* body;
	sf::Vector2i prev;

	float dt;
	int jumping;
	bool fly; 
	float grav_factor;

};
