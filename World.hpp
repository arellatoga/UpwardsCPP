#pragma once

#include <SFML/Graphics.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include "Entity.hpp"

class World {

public:
	World(int gravity);
	Entity* addEntity(int x, int y, int w, int h);
	Entity* addEntity(int x, int y, int w, int h, std::string sprite);
	void addEntity(Entity* body);
	
	// REMOVE APPLYGRAVITY LATER
	sf::Vector2i move(Entity*, int, int);
	sf::Vector2i move(Entity*, sf::Vector2i);
	sf::Vector2i ApplyGravity(Entity*);
	sf::Vector2i Gravity();
	sf::Vector2i TerminalVelocity();
	sf::Vector2i AppliedDelta(sf::Vector2i);

	bool IsGrounded(Entity*);
	float deltaTime();

private:
	std::vector<Entity*> entities;
	int gravity;
	int terminalVelocity;
};

