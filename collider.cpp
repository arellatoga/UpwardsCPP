#include "collider.hpp"

Collider::Collider() {
}

Collider::~Collider() {
	for (int i = 0; i < bodies.size(); i++) {
		delete bodies.at(i);
	}
}

Body* Collider::addBody(sf::Vector2i c, int _w, int _h, float _v, std::string _name, std::string _type) {
	Body* body = new Body(c, _w, _h, _v, _name, _type);
	bodies.push_back(body);
	return body;
}

sf::Vector2i Collider::move(Body* body, int x, int y, int& collision) {
	Body* temp; 

	int oy = body->r.top;
	int ox = body->r.left;

	for (int i = 0; i < bodies.size(); i++) {
		temp = bodies[i];

		// Simple AABB
		if (body != temp) {
			for (float j = 1.00; j <= 1; j += 1) {
				body->r.top = oy + j*y;
				body->r.left = ox + j*x;

				Body* hsweep = new Body(ox, oy, j*x + body->r.width, body->r.height);
				Body* vsweep = new Body(ox, oy, body->r.width, j*y + body->r.height);

				std::cout<<"y "<< y <<std::endl;

				if (body->r.intersects(temp->r)/*||body->r.intersects(hsweep->r)||body->r.intersects(vsweep->r)*/) {
					body->r.top = (body->r.top - j*y);
					body->r.left = (body->r.left - j*x);
			
					return sf::Vector2i(0, 0);
				}
			}
		}
	}

	return sf::Vector2i(x, y);
}
