#include "structures.hpp"

Body::Body(sf::Vector2i _c, int _w, int _h, float _v, std::string _name, std::string _type) {
	r = sf::IntRect(_c.x, _c.y, _w, _h);
	name = _name;
	type = _type;
	v = _v;
}

Body::Body(int _x, int _y, int _w, int _h) {
	r = sf::IntRect(_x, _y, _w, _h);
	name = "";
	type = "";
	v = 0;
}

