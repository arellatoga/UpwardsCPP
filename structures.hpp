#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <string>

class Body {
public:
	Body(sf::Vector2i, int, int, float, std::string, std::string);
	Body(int, int, int, int);
	std::string name;
	std::string type;
	float v;
	sf::IntRect r;
};

