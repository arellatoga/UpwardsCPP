#include "object.hpp"

StaticObject::StaticObject(Collider* co) {
	col = co;
	body = col->addBody(sf::Vector2i(0, 500), 800, 10, 0, "object", "object");

	sprite.setPosition(sf::Vector2f(body->r.left, body->r.top));
	sprite.setSize(sf::Vector2f(800, 10));
	sprite.setFillColor(sf::Color::White);

}

void StaticObject::draw(sf::RenderWindow& window) {
	window.draw(sprite);
}

