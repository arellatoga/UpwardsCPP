#include "Component.hpp"
#include "GameObject.hpp"

void GravityComponent::update(GameObject& go) {
	if (go.velocity.y < go.world.Gravity().y) {
		go.velocity += go.world.AppliedDelta(go.world.Gravity());
	}
	// Remove Gravity if object is touching something
	if (go.world.IsGrounded(go.body)) {
		go.velocity = sf::Vector2i(go.velocity.x, 0);
		go.grounded = true;
	}

	// Movement
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && go.grounded) {
		go.velocity += sf::Vector2i(0, -20);
		go.grounded = false;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		if (go.velocity.x > -10){
			go.velocity += go.world.AppliedDelta(sf::Vector2i(-20, 0));
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		if (go.velocity.x < 10) {
			go.velocity += go.world.AppliedDelta(sf::Vector2i(20, 0));
		}
	}
	else {
		go.velocity = sf::Vector2i(0, go.velocity.y);
	}

	sf::Vector2f dpg = sf::Vector2f(go.world.move(go.body, go.velocity));
	go.sprite.move(dpg);
	go.UpdateObject();
}
