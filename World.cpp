#include "World.hpp"

World::World(int gravity) {
	this->gravity = gravity;
	//entities = {};
}

Entity* World::addEntity(int x, int y, int w, int h, std::string type) {
	Entity* body = new Entity(x, y, w, h, type);
	entities.push_back(body);

	std::cout << "Added " << body->id() << std::endl;
	return body;
}

void World::addEntity(Entity* body) {
	entities.push_back(body);
}

bool World::IsGrounded(Entity* body) {
	Entity* temp;
	sf::IntRect b(body->coordinates() + sf::Vector2i(0, 1), body->size());

	for (int i = 0; i < entities.size(); i++) {
		temp = entities.at(i);
		if (body != temp) {
			sf::IntRect t(temp->coordinates(), temp->size());
			if (b.intersects(t)) {
				if (body->coordinates().y + body->size().y == temp->coordinates().y) {
					return true;
				}
			}
		}
	}

	return false;
}

sf::Vector2i World::move(Entity* body, int newx, int newy) {
	Entity* temp;
	sf::IntRect b(sf::Vector2i(newx, newy)+body->coordinates(), body->size());

	for (int i = 0; i < entities.size(); i++) {
		temp = entities.at(i);
		if (body != temp) {
			sf::IntRect t(temp->coordinates(), temp->size());
			if (b.intersects(t)) {
				int dx;
				int dy;
				int rx = 0;
				int ry = 0;

				if (newx > 0) {
					dx = t.left - (body->coordinates().x + body->size().x);
					if (dx > 0 && dx < newx) {
						rx = dx;
					}
				}
				else if (newx < 0) {
					dx = (t.left + t.width) - body->coordinates().x;
					if (dx < 0 && dx > newx) {
						rx = dx;
					}
				}

				if (newy > 0) {
					dy = t.top - (body->coordinates().y + body->size().y);
					if (dy > 0 && dy < newy) {
						ry = dy;
					}
				}
				else if (newy < 0) {
					dy = (t.top + t.height) - body->coordinates().y;
					if (dy < 0 && dy > newy) {
						ry = dy;
					}
				}

				if ((dx > 0 && newx < 0) || (dy < 0 && newx > 0)) {
					rx = newx;
				}
				if ((dy > 0 && newy < 0) || (dy < 0 && newy > 0)) {
					ry = newy;
				}

				body->AddCoordinates(sf::Vector2i(rx, ry));
				return sf::Vector2i(rx, ry);
			}
		}
	}

	sf::Vector2i r = sf::Vector2i(newx, newy) - body->coordinates();
	body->AddCoordinates(sf::Vector2i(newx, newy));

	return sf::Vector2i(newx, newy);
}

sf::Vector2i World::move(Entity* body, sf::Vector2i vec) {
	return move(body, vec.x, vec.y);
}

sf::Vector2i World::ApplyGravity(Entity* body) {
	float dt = 1.0/60.0;
	return move(body, 0, 1.0/60.0 * gravity > 1 ? 1.0/60.0 * gravity : 1);
}

sf::Vector2i World::Gravity() {
	return sf::Vector2i(0, gravity);
}

sf::Vector2i World::TerminalVelocity() {
	return sf::Vector2i(0, 150);
}

sf::Vector2i World::AppliedDelta(sf::Vector2i vec) {
	float x = 0;
	float y = 0;

	if (vec.x != 0) {
		x = std::abs(vec.x * deltaTime()) > 1.0 ? vec.x * deltaTime() : (vec.x > 0 ? 1 : -1);
	}
	if (vec.y != 0) {
		y = std::abs(vec.y * deltaTime()) > 1.0 ? vec.y * deltaTime() : (vec.y > 0 ? 1 : -1);
	}

	return sf::Vector2i(x, y);
}

float World::deltaTime() {
	return 1.0/60.0;
}
