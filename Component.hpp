#include <iostream>
#include <string>
#include <vector>

class GameObject;

class Component {
public:
	Component();
	//virtual ~Component() = 0;
	virtual void update(GameObject&) = 0;
};

class GravityComponent : public Component {
public:
	virtual void update(GameObject&);
};
