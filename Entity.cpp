/** ENTITY CLASS
 * Basic physics object with the following attributes:
 * x-coordinate
 * y-coordinate
 * width
 * height
 * unique ID
 * type
 **/

#include "Entity.hpp"

Entity::Entity(int x, int y, int w, int h, std::string t) {
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->type = t;
	ID = "";

	//srand (time(NULL));
	for (int i = 0; i < 10; i++) {
		int c = 33 + rand()%93;
		ID.push_back(c);
	}
}

sf::Vector2i Entity::coordinates() {
	return sf::Vector2i(x, y);
}

sf::Vector2i Entity::size() {
	return sf::Vector2i(w, h);
}

std::string Entity::id() {
	return ID;
}

sf::Vector2i Entity::AddCoordinates(sf::Vector2i v) {
	this->x += v.x;
	this->y += v.y;
	
	return sf::Vector2i(x, y);
}
