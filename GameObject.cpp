#include "Component.hpp"
#include "GameObject.hpp"

GameObject::GameObject(World& wo, int x, int y, int w, int h, std::string type) : world(wo){
	//world = wo;
	body = world.addEntity(x, y, w, h, type);

	sprite.setSize(sf::Vector2f(body->size()));
	sprite.setPosition(sf::Vector2f(body->coordinates()));
	sprite.setFillColor(sf::Color::Green);

	velocity = sf::Vector2i(0, 0);
	EquateToBody();
}

GameObject::~GameObject() {
	delete body;
}

void GameObject::EquateToBody() {
	x = body->coordinates().x;
	y = body->coordinates().y;
	w = body->size().x;
	h = body->size().y;
}

void GameObject::UpdateObject() {
	x = sprite.getPosition().x;
	y = sprite.getPosition().y;
}

void GameObject::run(bool grav) {
	// Gravity, do not touch!!
	if (grav) {
		// Apply Gravity
		if (velocity.y < world.Gravity().y) {
			velocity += world.AppliedDelta(world.Gravity());
		}
		// Remove Gravity if object is touching something
		if (world.IsGrounded(body)) {
			velocity = sf::Vector2i(velocity.x, 0);
			grounded = true;
		}

		// Movement
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && grounded) {
			velocity += sf::Vector2i(0, -20);
			grounded = false;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
			if (velocity.x > -10){
				velocity += world.AppliedDelta(sf::Vector2i(-20, 0));
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
			if (velocity.x < 10) {
				velocity += world.AppliedDelta(sf::Vector2i(20, 0));
			}
		}
		else {
			velocity = sf::Vector2i(0, velocity.y);
		}

		sf::Vector2f dpg = sf::Vector2f(world.move(body, velocity));
		sprite.move(dpg);
		UpdateObject();
	}
}

sf::Vector2i GameObject::coordinates() {
	return sf::Vector2i(x, y);
}

