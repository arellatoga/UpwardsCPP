#pragma once

#include <SFML/Graphics.hpp>

#include "World.hpp"
#include "Entity.hpp"

class Component;

class GameObject {
public:
	// Constructor
	GameObject(World& wo, int x, int y, int w, int h, std::string type);
	// Destructor - delete pointers
	~GameObject();
	// Called every frame
	void run(bool);
	// Sprite, move to private, create draw() function
	sf::RectangleShape sprite;
	sf::Vector2i coordinates();
	sf::Vector2i size();
	// x and y positions
	int x;
	int y;
	// width and height
	int w;
	int h;
	// is this object grounded?
	bool grounded;
	// velocity
	sf::Vector2i velocity;
	// physics body
	Entity* body;
	// pointer to world
	World& world;
	void AddComponent(Component&);
	void UpdateObject();
	void EquateToBody();
private:
};

