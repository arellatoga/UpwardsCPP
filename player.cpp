#include "player.hpp"

Player::Player(Collider* co){
	grav_factor = 0;

	sprite.setFillColor(sf::Color::Red);

	col = co;
	body = col->addBody(sf::Vector2i(0, 0), 64, 64, 350, "player", "player");
	sprite.setSize(sf::Vector2f(64, 64));
	sprite.setPosition(body->r.left, body->r.top);
	
	jumping = 1;
	fly = false;
}

void Player::draw(sf::RenderWindow& window) {
	window.draw(sprite);
}

void Player::run(sf::Event& event) {
	float _x = 0;
	float _y = 0;

	dt = 1.0/60.0;
	
	// Physics related
	std::cout << "jumpan " << jumping << std::endl;
	/*	
	if (jumping > 0) {
		grav_factor = 0;
		jumping = jumping > 0 ? jumping - 1 : 0;
	}
	else if (grav_factor < 0.0) {
		jumping = 1;
	}
	if (grav_factor >= -3) {
		std::cout << "Accelerating...\n";
		grav_factor -= 0.1;
	}
	*/
	
	if (grav_factor <= 0 && jumping == 2) {
		jumping = jumping > 0 ? jumping - 1 : 0;
	}
	else if (jumping == 1) {
		grav_factor -= 0.1;
	}
	else if (prev.y >= body->r.top) {
		grav_factor = 0; 
		jumping = jumping > 0 ? jumping - 1 : 0;
	}

	// Input related
	if (!fly) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
			_x = -1;
			sprite.setFillColor(sf::Color::Red);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
			_x = 1;
			sprite.setFillColor(sf::Color::Green);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && jumping <= 0) {
			_y = 1;
			jumping = 2;
			grav_factor = 3;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
			_y = -1;
		}
		move(_x, _y);
		//std::cout << "grav" << grav_factor << std::endl;
		move(0, grav_factor);
	}
	else if (fly) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
			_x = -1;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
			_x = 1;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
			_y = 1;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
			_y = -1;
		}
		move(_x, _y);
	}

	if (event.key.code == sf::Keyboard::F) {
		fly = fly ? false : true;	
	}
}

void Player::move(float _x, float _y) {
	int collisionInfo;
	int movx;
	int movy;

	prev = sf::Vector2i(body->r.left, body->r.top);
	_y = -_y;
	collisionInfo = 0;
	sf::Vector2i offset = col->move(body, _x * body->v * dt, _y * body->v * dt, collisionInfo);
	movx = offset.x;
	movy = offset.y;
	//std::cout << "Actual pos: " << body->r.left << " " << body->r.top << std::endl;
	sprite.move(movx, movy);
	//std::cout << "Sprite pos: " << sprite.getPosition().x << " " << sprite.getPosition().y << std::endl;
}

Body* Player::returnBody() {
	return body;
}
