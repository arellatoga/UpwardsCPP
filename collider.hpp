#pragma once

#include <vector>
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <cmath>
#include "structures.hpp"
#include <SFML/System.hpp>

class Collider {
public:
	Collider();
	~Collider();
	Body* addBody(sf::Vector2i, int, int, float, std::string, std::string);
	sf::Vector2i move(Body*, int, int, int&);
	float GRAVITY = 500;

private:
	std::vector<Body*> bodies;
}; 
