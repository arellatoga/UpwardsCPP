#pragma once

#include <SFML/Graphics.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <ctime>

class Entity {

public:
	Entity(int x, int y, int w, int h, std::string t);
	sf::Vector2i coordinates();
	sf::Vector2i UpdateCoordinates(sf::Vector2i);
	sf::Vector2i AddCoordinates(sf::Vector2i);
	sf::Vector2i size();
	std::string id();
	
private:
	int x;
	int y;
	int w;
	int h;
	std::string ID;
	std::string type; // Type of entitiy (player, lalilulelo)
};
