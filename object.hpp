#pragma once

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "collider.hpp"

class StaticObject {
public:
	StaticObject(Collider*);

	int getWidth();
	int getHeight();

	void draw(sf::RenderWindow&);

private:
	Body* body;
	Collider* col;

	sf::RectangleShape sprite;
};
