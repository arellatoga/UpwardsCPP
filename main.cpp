#include <iostream>
#include <string>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include "World.hpp"
#include "Entity.hpp"
#include "GameObject.hpp"
#include "camera.hpp"
#include "Component.hpp"

sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Works");

void initializeWindow(sf::RenderWindow&);

int main() {
	srand(time(NULL));
	// Init window specifics
	initializeWindow(window);
	World w(100);
	Camera cam;
	GameObject gameObject1(w, 0, 0, 100, 100, "player");
	GameObject gameObject2(w, 0, 600, 500, 100, "thing");
	GameObject gameObject3(w, 200, 500, 50, 75, "wall");

	GravityComponent grav;
	Component& comp = grav;
	gameObject1.AddComponent(comp);

	while (window.isOpen()) {
		sf::Event event;

		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}
		
		// Run stuff...
	
		cam.set(window);
		cam.follow(gameObject1);	

		gameObject1.run(true);
		gameObject2.run(false);
		gameObject3.run(false);

		window.clear();

		// Draw stuff...
		
		window.draw(gameObject1.sprite);
		window.draw(gameObject2.sprite);
		window.draw(gameObject3.sprite);

		window.display();

		window.setView(window.getDefaultView());
		// GUI stuff

	}

	return 0;
}

void initializeWindow(sf::RenderWindow& window) {
	window.setFramerateLimit(60);
}
